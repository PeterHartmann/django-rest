from snippets.models import Snippet
from snippets.serializers import SnippetSerializer
from snippets.serializers import UserSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework import permissions
from snippets.permissions import IsOwnerOrReadOnly
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response


class SnippetViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """

    serializer_class = SnippetSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    renderer_classes = [renderers.TemplateHTMLRenderer]
    template_name = "snippets/snippet-list.html"

    def list(self, request):
        queryset = Snippet.objects.all()
        return Response(
            data={"snippets": queryset, "serializer": self.serializer_class}
        )

    def create(self, request):
        serializer = SnippetSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save(owner=request.user)
        queryset = Snippet.objects.all()
        return Response(
            data={"snippets": queryset, "serializer": self.serializer_class},
        )

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """

    queryset = User.objects.all()
    serializer_class = UserSerializer
    # renderer_classes = [renderers.TemplateHTMLRenderer]
